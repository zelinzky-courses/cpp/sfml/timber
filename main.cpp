#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <sstream>

const int NUM_BRANCHES = 6;

sf::Sprite branches[NUM_BRANCHES];

enum class side {
    LEFT, RIGHT, NONE
};

side branchPositions[NUM_BRANCHES];

void updateBranches(int seed) {
    for (int j = NUM_BRANCHES; j > 0; j--) {
        branchPositions[j] = branchPositions[j - 1];
    }
    srand((int) time(0) + seed);
    int randomGen = (rand() % 5);
    switch (randomGen) {
        case 0 :
            branchPositions[0] = side::LEFT;
            break;
        case 1 :
            branchPositions[0] = side::RIGHT;
            break;
        default:
            branchPositions[0] = side::NONE;
    }

}


int main() {
    // Create video mode object
    sf::VideoMode vm{1920, 1080};
    // Create and open full screen window
    sf::RenderWindow window{vm, "Timber!!!", sf::Style::Fullscreen};

    // Create background texture object and load it from file
    sf::Texture textureBackground;
    textureBackground.loadFromFile("graphics/background.png");

    // Create background sprite from background texture
    sf::Sprite spriteBackground;
    spriteBackground.setTexture(textureBackground);
    spriteBackground.setPosition(0, 0);

    // Create tree sprite
    sf::Texture textureTree;
    textureTree.loadFromFile("graphics/tree.png");
    sf::Sprite spriteTree;
    spriteTree.setTexture(textureTree);
    spriteTree.setPosition(810, 0);

    // Create bee sprite
    sf::Texture textureBee;
    textureBee.loadFromFile("graphics/bee.png");
    sf::Sprite spriteBee;
    spriteBee.setTexture(textureBee);
    spriteBee.setPosition(0, 800);
    bool beeActive = false;
    float beeSpeed = 0.0f;

    // Create cloud sprites
    sf::Texture textureCloud;
    textureCloud.loadFromFile("graphics/cloud.png");
    sf::Sprite spriteCloud1, spriteCloud2, spriteCloud3;
    spriteCloud1.setTexture(textureCloud);
    spriteCloud2.setTexture(textureCloud);
    spriteCloud3.setTexture(textureCloud);

    spriteCloud1.setPosition(0, 0);
    spriteCloud2.setPosition(0, 250);
    spriteCloud3.setPosition(0, 500);

    bool cloud1Active = false;
    bool cloud2Active = false;
    bool cloud3Active = false;

    float cloud1Speed = 0.0f;
    float cloud2Speed = 0.0f;
    float cloud3Speed = 0.0f;

    sf::Clock clock;

    // Time bar
    sf::RectangleShape timeBar;
    float timeBarInitialWidth = 400;
    float timeBarHeight = 80;
    timeBar.setSize(sf::Vector2f(timeBarInitialWidth, timeBarHeight));
    timeBar.setFillColor(sf::Color::Red);
    timeBar.setPosition((1920 / 2) - timeBarInitialWidth / 2, 980);
    sf::Time gameTimeTotal;
    float timeRemaining = 6.0;
    float timeBarWidthPerSecond = timeBarInitialWidth / timeRemaining;


    bool paused = true;

    int score = 0;

    sf::Text messageText;
    sf::Text scoreText;

    sf::Font font;
    font.loadFromFile("fonts/KOMIKAP_.ttf");

    messageText.setFont(font);
    scoreText.setFont(font);

    messageText.setString("Press enter to start!");
    scoreText.setString("Score = 0");
    messageText.setCharacterSize(75);
    scoreText.setCharacterSize(100);
    messageText.setFillColor(sf::Color::White);
    scoreText.setFillColor(sf::Color::White);

    sf::FloatRect textRect = messageText.getLocalBounds();
    messageText.setOrigin(textRect.left + textRect.width / 2.0, textRect.top + textRect.height / 2.0);
    messageText.setPosition(1920 / 2., 1080 / 2.);
    scoreText.setPosition(20, 20);

    // Branches
    sf::Texture textureBranch;
    textureBranch.loadFromFile("graphics/branch.png");

    for (int i = 0; i < NUM_BRANCHES; i++) {
        branches[i].setTexture(textureBranch);
        branches[i].setPosition(-2000, -2000);
        //Set sprite origin to the centre
        branches[i].setOrigin(220, 20);
    }


    // Prepare the player

    sf::Texture texturePlayer;
    texturePlayer.loadFromFile("graphics/player.png");
    sf::Sprite spritePlayer;
    spritePlayer.setTexture(texturePlayer);
    spritePlayer.setPosition(580, 720);
    // Player should start on the left'
    side playerSide = side::LEFT;
    // Prepare gravestone
    sf::Texture textureRIP;
    textureRIP.loadFromFile("graphics/rip.png");
    sf::Sprite spriteRIP;
    spriteRIP.setTexture(textureRIP);
    spriteRIP.setPosition(600, 860);


    // Prepare the axe
    sf::Texture textureAxe;
    textureAxe.loadFromFile("graphics/axe.png");
    sf::Sprite spriteAxe;
    spriteAxe.setTexture(textureAxe);
    spriteAxe.setPosition(700, 830);
    // Line the axe up with the tree
    const float AXE_POSITION_LEFT = 700;
    const float AXE_POSITION_RIGHT = 1075;
    // Prepare flying log
    sf::Texture textureLog;
    textureLog.loadFromFile("graphics/log.png");
    sf::Sprite spriteLog;
    spriteLog.setTexture(textureLog);
    spriteLog.setPosition(810, 720);
    // Other log variables.
    bool logActive = false;
    float logSpeedX = 1000;
    float logSpeedY = -1500;


    // Control player input
    bool acceptInput = false;

    // prepare audio fx
    sf::SoundBuffer chopBuffer;
    if (!chopBuffer.loadFromFile("sound/chop.wav")) {
        return -1;
    }
    sf::Sound chop;
    chop.setBuffer(chopBuffer);
    sf::SoundBuffer deathBuffer;
    deathBuffer.loadFromFile("sound/death.wav");
    sf::Sound death;
    death.setBuffer(deathBuffer);
    sf::SoundBuffer ootBuffer;
    ootBuffer.loadFromFile("sound/out_of_time.wav");
    sf::Sound outOfTime;
    outOfTime.setBuffer(ootBuffer);



    while (window.isOpen()) {
        // Handle players input

        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::KeyReleased && !paused) {
                acceptInput = true;
                spriteAxe.setPosition(2000, spriteAxe.getPosition().y);
            }
        }


        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            window.close();
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter)) {
            paused = false;
            // reset score and time
            score = 0;
            timeRemaining = 6;

            // make all branches dissappear
            for (int i = 1; i < NUM_BRANCHES; ++i) {
                branchPositions[i] = side::NONE;
            }
            // Hide gravestone
            spriteRIP.setPosition(675, 2000);
            // set player into position
            spritePlayer.setPosition(580, 720);
            acceptInput = true;
        }

        if (acceptInput) {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
                playerSide = side::RIGHT;
                score++;
                timeRemaining += (2 / score) + .15;
                spriteAxe.setPosition(AXE_POSITION_RIGHT, spriteAxe.getPosition().y);
                spritePlayer.setPosition(1200, 720);
                // update branches
                updateBranches(score);

                // set the log flying to  the left
                spriteLog.setPosition(810, 720);
                logSpeedX = -5000;
                logActive = true;
                acceptInput = false;
                chop.play();
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
                playerSide = side::LEFT;
                score++;
                timeRemaining += (2 / score) + .15;
                spriteAxe.setPosition(AXE_POSITION_LEFT, spriteAxe.getPosition().y);
                spritePlayer.setPosition(520, 720);
                // update branches
                updateBranches(score);

                // set the log flying to  the left
                spriteLog.setPosition(810, 720);
                logSpeedX = 5000;
                logActive = true;
                acceptInput = false;
                chop.play();
            }
        }


        // Update scene

        if (!paused) {
            sf::Time dt = clock.restart();

            timeRemaining -= dt.asSeconds();
            timeBar.setSize(sf::Vector2f(timeBarWidthPerSecond * timeRemaining, timeBarHeight));

            // Out of time!
            if (timeRemaining < 0.0) {
                paused = true;
                messageText.setString("Out of time!!");
                // Reposition new message text
                sf::FloatRect textRect = messageText.getLocalBounds();
                messageText.setOrigin(textRect.left + textRect.width / 2.0, textRect.top + textRect.height / 2.0);
                outOfTime.play();
            }

            // Bee

            if (!beeActive) {
                srand((int) time(nullptr));
                beeSpeed = (rand() % 200) + 200;
                float height = (rand() % 500) + 500;
                spriteBee.setPosition(2000, height);
                beeActive = true;
            } else {
                spriteBee.setPosition(spriteBee.getPosition().x - (beeSpeed * dt.asSeconds()),
                                      spriteBee.getPosition().y);
                if (spriteBee.getPosition().x < -100) {
                    beeActive = false;
                }
            }

            // Clouds
            // Cloud1
            if (!cloud1Active) {
                srand((int) time(nullptr) * 10);
                cloud1Speed = (rand() % 200);
                float height = (rand() % 150);
                spriteCloud1.setPosition(-200, height);
                cloud1Active = true;
            } else {
                spriteCloud1.setPosition(spriteCloud1.getPosition().x + (cloud1Speed * dt.asSeconds()),
                                         spriteCloud1.getPosition().y);
                if (spriteCloud1.getPosition().x > 1920) {
                    cloud1Active = false;
                }
            }

            // Cloud2
            if (!cloud2Active) {
                srand((int) time(nullptr) * 20);
                cloud2Speed = (rand() % 200);
                float height = (rand() % 150);
                spriteCloud2.setPosition(-200, height);
                cloud2Active = true;
            } else {
                spriteCloud2.setPosition(spriteCloud2.getPosition().x + (cloud2Speed * dt.asSeconds()),
                                         spriteCloud2.getPosition().y);
                if (spriteCloud2.getPosition().x > 1920) {
                    cloud2Active = false;
                }
            }

            // Cloud3
            if (!cloud3Active) {
                srand((int) time(nullptr) * 30);
                cloud3Speed = (rand() % 200);
                float height = (rand() % 150);
                spriteCloud3.setPosition(-200, height);
                cloud3Active = true;
            } else {
                spriteCloud3.setPosition(spriteCloud3.getPosition().x + (cloud3Speed * dt.asSeconds()),
                                         spriteCloud3.getPosition().y);
                if (spriteCloud3.getPosition().x > 1920) {
                    cloud3Active = false;
                }
            }

            // update score text
            std::stringstream scoreString;
            scoreString << "Score = " << score;
            scoreText.setString(scoreString.str());

            // update branch sprites
            for (int i = 0; i < NUM_BRANCHES; i++) {
                float height = i * 150;

                switch (branchPositions[i]) {
                    case side::LEFT:
                        branches[i].setPosition(610, height);
                        branches[i].setRotation(180);
                        break;
                    case side::RIGHT:
                        branches[i].setPosition(1330, height);
                        branches[i].setRotation(0);
                        break;
                    default:
                        branches[i].setPosition(3000, height);
                }
            }

            // handle flying log
            if (logActive) {
                spriteLog.setPosition(spriteLog.getPosition().x + (logSpeedX * dt.asSeconds()),
                                      spriteLog.getPosition().y + (logSpeedY * dt.asSeconds()));
                // has the log gotten out of screen
                if (spriteLog.getPosition().x < -100 || spriteLog.getPosition().x > 2000) {
                    logActive = false;
                    spriteLog.setPosition(810, 720);
                }
            }
//
//            // Has the player died?
            if (branchPositions[5] == playerSide) {
                // he died
                paused = true;
                acceptInput = false;
                spriteRIP.setPosition(525, 760);
                spritePlayer.setPosition(2000, 660);
                messageText.setString("SQUISHED!!");
                sf::FloatRect textRe = messageText.getLocalBounds();
                messageText.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
                messageText.setPosition(1920/2.0f, 1080/2.0f);
                death.play();
            }

        }

        // Draw the scene
        // Clear last frame
        window.clear();
        // Draw new scene
        window.draw(spriteBackground);
        window.draw(spriteCloud1);
        window.draw(spriteCloud2);
        window.draw(spriteCloud3);

        // Draw the branches
        for (int i = 0; i < NUM_BRANCHES; ++i) {
            window.draw(branches[i]);
        }

        window.draw(spriteTree);

        // Draw interactive objects
        window.draw(spritePlayer);
        window.draw(spriteAxe);
        window.draw(spriteLog);
        window.draw(spriteRIP);


        window.draw(spriteBee);
        // Draw the score
        window.draw(scoreText);
        window.draw(timeBar);
        if (paused) {
            window.draw(messageText);
        }
        // Display scene drawn
        window.display();
    }

    return 0;
}
